import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';;
import { BehaviorSubject } from 'rxjs';
import { Platform, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  authenticationState = new BehaviorSubject(false);

  tokenInterval:any;
  timeInterval: any;
  loggedIn: boolean = false;

  protocolKC: string = 'https://';
  keycloakURL: string = 'auth.colmakers.com/auth/realms/';

  username: string = "";
  password: string = "";
  token: any;
  refresh_token:any;
  token_exp:any;
  expires_in:any;
  refresh_expires_in:any;
  client_id: string = "lucy4-app";
  constructor(
    private http: Http,
    private platform: Platform,
    public toast: ToastController,
    private router: Router,
  ) {
    this.platform.ready().then(() => {
      this.checkToken();
    });
  }

  checkInternet() {
    return this.http.get('https://api.colmakers.com').pipe(timeout(4000));
  }


  checkToken() {
    this.token = localStorage.getItem("access_token");
    this.refresh_token = localStorage.getItem("refresh_token");
    this.token_exp = JSON.parse(localStorage.getItem("expires_at"));
    this.expires_in = JSON.parse(localStorage.getItem("expires_in"));
    this.refresh_expires_in = JSON.parse(localStorage.getItem("refresh_expires_in"));

    if (this.token) {
      this.loggedIn = Date.now() < JSON.parse(this.token_exp);
      console.log("logged: ", this.loggedIn);

      if (this.loggedIn) {
        this.refreshToken().subscribe(data => {
          this.saveToken(data.json());
        });
        this.createInterval(this.expires_in * 1000);
        this.authenticationState.next(true);
      } else {
        if (this.refresh_expires_in == 0) {
          this.checkInternet().subscribe(data=> {
            this.authenticationState.next(false);
            this.deleteToken();
          }, err => {
            this.authenticationState.next(true);
          });
        } else {
          this.authenticationState.next(false);
          this.refreshToken().subscribe(data => {
            let res = data.json();
            this.saveToken(res);
            this.createInterval(res.expires_in * 1000);
            this.authenticationState.next(true);
          }, err => {
            if (this.refresh_expires_in != 0) {
              this.authenticationState.next(false);
              this.deleteToken();
            }
          });
        }
      }
    } else {
      this.authenticationState.next(false);
    }
  }

  login(offline) {
    let params = new URLSearchParams();
    if (offline) {
      params.append('scope', 'offline_access');
    }
    params.append('username', this.username);
    params.append('password', this.password);
    params.append('grant_type', 'password');
    params.append('client_id', this.client_id);

    let headers: Headers = new Headers;
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post(this.protocolKC + this.keycloakURL + 'colmakers/protocol/openid-connect/token', params.toString(), { headers: headers}).subscribe(data => {
      let res = data.json();
      this.loggedIn = true;
      this.saveToken(res);
      this.username = "";
      this.password = "";
      this.authenticationState.next(true);
    }, err => {
      let errRes = err.json();
      if (errRes.error_description == "Invalid user credentials") {
        this.password = "";
        this.presentToast("Correo o contraseña invalidas");
      }
    });
  }

  createInterval(timeInterval) {
    this.timeInterval = timeInterval;
    this.tokenInterval = setInterval(() => {
      this.refreshToken().subscribe(data => {
        this.saveToken(data.json());
      }, err => {
        if (this.refresh_expires_in != 0) {
          this.authenticationState.next(false);
          this.deleteToken();
        }
      });
    }, this.timeInterval);
  }

  refreshToken() {
    let refreshToken = localStorage.getItem("refresh_token");
    let params = new URLSearchParams();
    params.append('grant_type', 'refresh_token');
    params.append('refresh_token', refreshToken);
    params.append('client_id', this.client_id);

    let headers: Headers = new Headers;
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    return this.http.post(this.protocolKC + this.keycloakURL + 'colmakers/protocol/openid-connect/token', params.toString(), { headers: headers});
  }

  saveToken(res) {
    let expiresAt = JSON.stringify((res.expires_in * 1000) + new Date().getTime());
    localStorage.setItem("access_token", res.access_token);
    localStorage.setItem("refresh_token", res.refresh_token);
    localStorage.setItem("expires_at", expiresAt);
    localStorage.setItem("expires_in", res.expires_in);
    localStorage.setItem("refresh_expires_in", res.refresh_expires_in);
    this.token = res.access_token;
    this.timeInterval = parseInt(res.expires_in) * 1000;
    if (!this.tokenInterval) {
      this.createInterval(this.timeInterval);
    }
  }

  deleteToken() {
    if (this.refresh_expires_in != 0) {
      localStorage.removeItem('access_token');
      localStorage.removeItem('refresh_token');
      localStorage.removeItem('expires_in');
      localStorage.removeItem('expires_at');
      localStorage.removeItem('refresh_expires_in');
      this.gotoLogin();
    }
  }

  gotoLogin() {
    this.router.navigate(['login']);
  }

  isAuthenticated() {
    return this.authenticationState.value;
  }

  async presentToast(message) {
    const toast = await this.toast.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  logout() {
    let refreshToken = localStorage.getItem("refresh_token");
    let params = new URLSearchParams();
    params.append('refresh_token', refreshToken);
    params.append('client_id', this.client_id);

    let headers: Headers = new Headers;
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    this.http.post(this.protocolKC + this.keycloakURL + 'colmakers/protocol/openid-connect/logout', params.toString(), { headers: headers}).subscribe(data => {
      localStorage.removeItem('access_token');
      localStorage.removeItem('devices');
      localStorage.removeItem('domo');
      localStorage.removeItem('domos');
      localStorage.removeItem('refresh_token');
      localStorage.removeItem('expires_in');
      localStorage.removeItem('expires_at');
      localStorage.removeItem('refresh_expires_in');
      //window.location.reload();
      this.gotoLogin();
    });
  }
}