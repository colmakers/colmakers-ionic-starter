import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AuthService } from '../services/auth.service';
import { timeout } from 'rxjs/operators';
import { ToastController } from '@ionic/angular';

@Injectable()
export class ColmakersService {
  pins: any = [
    {name:"Da Vinci", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d4", show: false, color: "medium"},
    {name:"Copérnico", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d5", show: false, color: "medium"},
    // {name:"Galileo", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d17", show: false, color: "medium"},
    {name:"Newton", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d23", show: false, color: "medium"},
    {name:"Darwin", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d24", show: false, color: "medium"},
    {name:"Edison", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d25", show: false, color: "medium"},
    {name:"Tesla", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d3", show: false, color: "medium"},
    {name:"Volta", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d2", show: false, color: "medium"},
    {name:"Faraday", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d1", show: false, color: "medium"},
    {name:"Curie", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d22", show: false, color: "medium"},
    {name:"Einstein", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d21", show: false, color: "medium"},
    {name:"Turing", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d20", show: false, color: "medium"}
  ];

  outputs = [
    {name:"Da Vinci", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d4", show: false, color: "medium", buttonColor: "medium", value: 0, prevValue: 0},
    {name:"Copérnico", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d5", show: false, color: "medium", buttonColor: "medium", value: 0, prevValue: 0},
    // {name:"Galileo", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d17", show: false, color: "medium", buttonColor: "medium", value: 0, prevValue: 0},
    {name:"Darwin", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d24", show: false, color: "medium", buttonColor: "medium", value: 0, prevValue: 0},
    {name:"Edison", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d25", show: false, color: "medium", buttonColor: "medium", value: 0, prevValue: 0},
    {name:"Tesla", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d3", show: false, color: "medium", buttonColor: "medium", value: 0, prevValue: 0},
    {name:"Volta", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d2", show: false, color: "medium", buttonColor: "medium", value: 0, prevValue: 0},
    {name:"Faraday", buzzerIcon: "volume-mute", buttonIcon: "radio-button-off", pin:"d1", show: false, color: "medium", buttonColor: "medium", value: 0, prevValue: 0},
  ];

  analog = [
    {name:"Newton", pin:"a4", show: false, value: 0},
    {name:"Darwin", pin:"a5", show: true, value: 0},
    {name:"Edison", pin:"a6", show: false, value: 0},
    {name:"Curie", pin:"a3", show: false, value: 0},
    {name:"Einstein", pin:"a2", show: false, value: 0},
    {name:"Turing", pin:"a1", show: false, value: 0}
  ];

  analogPins:any = [];
  // urlApi: string = "192.168.1.109:3000";
  // protocol: string = "http://";
  //lucyIp: string = "192.168.1.100";
  lucys: any = [];
  lucyIp: string = "192.168.4.1";
  buttonsToShow: any = [];
  buzzersToShow: any = [];
  potesToShow: any = [];
  photoToShow: any = [];
  servosToShow: any = [];
  soundsToShow: any = [];
  uvsToShow: any = [];
  DHTpin: any = null;
  mode: any = "remote";
  serial: any = null;

  urlApi: string = "api.colmakers.com";
  protocol: string = "https://";

  constructor(
    private http: Http,
    private auth: AuthService,
    private toast: ToastController
  ) {

  }

  checkInternet() {
    return this.http.get('https://api.colmakers.com').pipe(timeout(3000));
  }

  createUser(user) {
    let body = user;
    return this.http.post(this.protocol + this.urlApi + '/colmakers/lucy4/user', body);
  }

  addLucy(lucy) {
    let headers: Headers = new Headers;
    headers.append('Authorization', 'Bearer ' + this.auth.token);
    let body = lucy;
    return this.http.post(this.protocol + this.urlApi + '/colmakers/lucy4/lucy', body, {headers:headers}).pipe(timeout(3000));
  }

  getLucys() {
    let headers: Headers = new Headers;
    headers.append('Authorization', 'Bearer ' + this.auth.token);
    return this.http.get(this.protocol + this.urlApi + '/colmakers/lucy4/lucys', {headers:headers}).pipe(timeout(3000));
  }

  getLucy(serial) {
    let headers: Headers = new Headers;
    headers.append('Authorization', 'Bearer ' + this.auth.token);
    return this.http.get(this.protocol + this.urlApi + '/colmakers/lucy4/lucy/'+serial, {headers:headers});
  }

  deleteLucy(serial) {
    let headers: Headers = new Headers;
    headers.append('Authorization', 'Bearer ' + this.auth.token);
    return this.http.delete(this.protocol + this.urlApi + '/colmakers/lucy4/lucy/'+serial, {headers:headers}).pipe(timeout(3000));
  }

  changeLucyName(data, serial) {
    let headers: Headers = new Headers;
    let body = {
      name: data.name
    }
    headers.append('Authorization', 'Bearer ' + this.auth.token);
    return this.http.post(this.protocol + this.urlApi + '/colmakers/lucy4/'+serial+'/name', body, {headers:headers}).pipe(timeout(3000));
  }

  checkDirect() {
    return this.http.get("http://" + this.lucyIp).pipe(timeout(2000));
  }

  status(ip) {
    return this.http.get('http://' + ip).pipe(timeout(2000));
  }

  scan() {
    return this.http.get("http://" + this.lucyIp + '/scan').pipe(timeout(5000));
  }

  connectWifi(ssid, pass) {
    let body = {
      ssid: ssid,
      password: pass
    }
    return this.http.post("http://" + this.lucyIp + '/connect', body);
  }

  setLEDColor(r, g, b) {
    if (this.mode == 'remote') {
      let headers: Headers = new Headers;
      headers.append('Authorization', 'Bearer ' + this.auth.token);
      let body = {
        r: r,
        g: g,
        b: b
      }
      return this.http.post(this.protocol + this.urlApi + '/colmakers/lucy4/' + this.serial + '/led', body, {headers:headers}).pipe(timeout(2000));
    } else {
      return this.http.get("http://" + this.lucyIp + '/led?r=' + r + "&g=" + g + "&b=" + b);
    }
  }

  turn(pin, state) {
    if (this.mode == 'remote') {
      let headers: Headers = new Headers;
      headers.append('Authorization', 'Bearer ' + this.auth.token);
      let body = {
        pin: pin,
        state: state
      }
      return this.http.post(this.protocol + this.urlApi + '/colmakers/lucy4/' + this.serial + '/turn', body, {headers:headers}).pipe(timeout(2000));
    } else {
      return this.http.get("http://" + this.lucyIp + '/turn?' + pin + '=' + state);
    }
  }

  modeInputs() {
    return this.http.get("http://" + this.lucyIp + '/modeinputs');
  }

  getDataDHT(pin) {
    if (this.mode == "remote") {
      let headers: Headers = new Headers;
      headers.append('Authorization', 'Bearer ' + this.auth.token);
      return this.http.get(this.protocol + this.urlApi + '/colmakers/lucy4/' + this.serial + '/dht?pin=' + pin, {headers:headers}).pipe(timeout(2000));
    } else {
      return this.http.get("http://" + this.lucyIp + '/dht?pin=' + pin)
    }
  }

  servo(pin, value) {
    if (this.mode == 'remote') {
      let headers: Headers = new Headers;
      headers.append('Authorization', 'Bearer ' + this.auth.token);
      let body = {
        pin: pin,
        value: value
      }
      return this.http.post(this.protocol + this.urlApi + '/colmakers/lucy4/' + this.serial + '/servo', body, {headers:headers}).pipe(timeout(2000));
    } else {
      return this.http.get("http://" + this.lucyIp + '/servo?' + pin + '=' + value)
    }
  }

  getInputs() {
    let headers: Headers = new Headers;
    headers.append('Authorization', 'Bearer ' + this.auth.token);
    return this.http.get(this.protocol + this.urlApi + '/colmakers/lucy4/' + this.serial + '/digitals', {headers:headers}).pipe(timeout(2000));
  }

  getAnalogs() {
    let headers: Headers = new Headers;
    headers.append('Authorization', 'Bearer ' + this.auth.token);
    return this.http.get(this.protocol + this.urlApi + '/colmakers/lucy4/' + this.serial + '/analogs', {headers:headers}).pipe(timeout(2000));
  }

  async presentToast(message) {
    const toast = await this.toast.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
}
