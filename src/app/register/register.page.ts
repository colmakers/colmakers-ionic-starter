import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { ColmakersService } from '../services/colmakers.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  user: any = {
    firstName: '',
    lastName: '',
    typeId: 'CC',
    documentId: '',
    phone: '',
    email: '',
    password: ''
  };
  confirmPass:string;
  disableBtn: boolean = false;
  iconPass: string = "eye";
  inputType: string = "password";
  constructor(
    private modal: ModalController,
    private toast: ToastController,
    private colmakers: ColmakersService
  ) { }

  ngOnInit() {
  }

  back() {
    this.modal.dismiss();
  }

  createUser() {
    if (this.user.firstName.length < 3) {
      this.presentToast('Ingrese un nombre');
    } else if (this.user.lastName.length < 3) {
      this.presentToast('Ingrese un apellido');
    } else if (this.user.email.length < 6 || this.user.email.indexOf("@") < 0) {
      this.presentToast('Ingrese un email valido');
    } else if (this.user.password.length < 6) {
      this.presentToast('La contraseña debe contener minimo 6 caracteres');
    } else {
      if (this.user.password === this.confirmPass) {
        this.disableBtn = true;
        this.colmakers.createUser(this.user).subscribe(data => {
          this.modal.dismiss();
          this.presentToast('Usuario creado con exito');
        }, err => {
          if (err.status == 400) {
            if (err.text() == 'User exists with same username') {
              this.disableBtn = false;
              this.user.password = '';
              this.confirmPass = '';
              this.presentToast('Este correo '+ this.user.email +' ya ha sido registrado');
            }
          }
        });
      } else {
        this.presentToast('Las contraseñas no coinciden');
      }
    }
  }

  showPass() {
    if (this.iconPass == "eye") {
      this.iconPass = "eye-off";
      this.inputType = "text";
    } else {
      this.iconPass = "eye";
      this.inputType = "password";
    }
  }

  async presentToast(message) {
    const toast = await this.toast.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
