import { Component, OnInit } from '@angular/core';
import { ToastController, ModalController } from '@ionic/angular';
import { RegisterPage } from '../register/register.page';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  iconPass: string = "eye";
  inputType: string = "password";
  token: string = "token aqui";
  tokenExp: any;
  loggedIn: any;
  intervalToken:any;
  rememberme: boolean = true;
  constructor(
    public auth: AuthService,
    private toastCtrl: ToastController,
    private modal: ModalController
  ) { }

  ngOnInit() {
  }

  login() {
    if (!this.auth.username) {
      this.presentToast("Proporciona un correo");
    } else if (!this.auth.password) {
      this.presentToast("Proporciona una contraseña");
    } else if (this.auth.password.length < 5) {
      this.presentToast("La contraseña tiene menos de 6 caracteres");
    } else if (this.auth.username && this.auth.password) {
      this.auth.login(this.rememberme);
    }
  }

  showPass() {
    if (this.iconPass == "eye") {
      this.iconPass = "eye-off";
      this.inputType = "text";
    } else {
      this.iconPass = "eye";
      this.inputType = "password";
    }
  }

  createAccount() {
    this.presentModal(RegisterPage, null);
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  async presentModal(page, data) {
    const modal = await this.modal.create({
      component: page,
      componentProps: {data: data}
    });
    modal.onDidDismiss().then(() => {
      //this.getDomos();
    });
    return await modal.present();
  }

}
