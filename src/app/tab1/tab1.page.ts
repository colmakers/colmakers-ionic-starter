import { Component } from '@angular/core';
import { ColmakersService } from '../services/colmakers.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  ws: any;
  davinci: string = "";
  buttonText: string = "Encender";
  buttonState: string = "";
  constructor(
    private colmakers: ColmakersService
  ) {

  }

  ionViewDidEnter() {
    this.ws = new WebSocket('ws://'+ this.colmakers.lucyIp + ":81", []);
    this.ws.onopen = () => {
      this.ws.send("digital");
    }
    this.ws.onmessage = (event) => {
      this.handleMessage(event);
    }
  }

  ionViewDidLeave() {
    this.ws.close();
  }

  turn() {
    let state = "";
    if (this.buttonState == "on") {
      state = "off";
    } else {
      state = "on"
    }
    this.colmakers.mode = "local";
    this.colmakers.turn("d3", state).subscribe(data => {

    });
  }

  handleMessage(event) {
    let resFromLucy = JSON.parse(event.data);
    this.davinci = resFromLucy["d4"];
    this.buttonState = resFromLucy["d3"];
    if (this.buttonState == "on") {
      this.buttonText = "Apagar";
    } else {
      this.buttonText = "Encender";
    }
  }
}
